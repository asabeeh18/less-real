package com.tct.less_real;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.Process;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    public static int start=-1;
    String url= computeURL();
    SharedPreferences pref;
    String user;
    int bec = 1;
   // ActionBar actionBar;
    MenuItem mn;
    Connect connectify;
    Context act;
    static int firstVisibleItem=0;
    protected static ProgressBar mProgress;
    ListView mainList,mDrawerList;
    DrawerLayout mDrawerLayout;
    ImageView les,rel;
    public static int count = 0;
    ViewFlipper flipper;
    TextView textView;
    ConnectivityManager cm;
    NetworkInfo ni;
    ArrayList<String> mTitle;
    boolean inv;
    MainFrag mf;
    ArrayList<Drit> drlist;
    private android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;

    /**
     * Dispatch onPause() to fragments.
     */
    @Override
    protected void onPause() {
        //firstVisibleItem=mainList.getFirstVisiblePosition();
        super.onPause();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_drawer);

        mTitle = new ArrayList<>();
        mTitle.add("Quotes");
        bec = getSupportFragmentManager().getBackStackEntryCount();

        mf = new MainFrag(getApplicationContext(),new Connect(mainList,this,getSupportActionBar()));

        SharedPreferences sp = getSharedPreferences("Settings",MODE_PRIVATE);

        start = sp.getInt("startID",-1);
        inv = sp.getBoolean("Inv",false);

        cm = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        ni = cm.getActiveNetworkInfo();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // Set the adapter for the list view

        drlist = new ArrayList<>();
        Drit dr1 = new Drit();
        dr1.setDrawableID(R.drawable.quote);
        dr1.setItemstr("Quotes");
        Drit dr2 = new Drit();
        dr2.setDrawableID(R.drawable.fav2);
        dr2.setItemstr("Favorites");
        Drit dr3 = new Drit();
        dr3.setDrawableID(R.drawable.settings);
        dr3.setItemstr("Settings");
        Drit dr4 = new Drit();
        dr4.setDrawableID(R.drawable.help);
        dr4.setItemstr("Help");
        Drit dr5 = new Drit();
        dr5.setDrawableID(R.drawable.about);
        dr5.setItemstr("About");

        drlist.add(dr1);
        drlist.add(dr2);
        drlist.add(dr3);
        drlist.add(dr4);
        drlist.add(dr5);

        Log.i("DR",Integer.toString(drlist.size()));

        DrawerAdapter drawerAdapter = new DrawerAdapter(this,drlist);

        mDrawerList.setAdapter(drawerAdapter);
        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                if(mTitle.size()!=0) {
                    getSupportActionBar().setTitle(mTitle.get(mTitle.size() - 1));
                }
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("Menu");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(android.R.drawable.ic_menu_more);


        final ViewFlipper viewFlipper = (ViewFlipper)findViewById(R.id.viewFlipper);
        RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.rell);

        if(inv)
        {
            mDrawerList.setBackgroundColor(getResources().getColor(R.color.background_floating_material_light));
            relativeLayout.setBackgroundColor(getResources().getColor(R.color.background_material_light));
            viewFlipper.setBackgroundColor(getResources().getColor(R.color.background_material_light));
        }
        else
        {
            mDrawerList.setBackgroundColor(getResources().getColor(R.color.background_floating_material_dark));
            relativeLayout.setBackgroundColor(getResources().getColor(R.color.background_material_dark));
            viewFlipper.setBackgroundColor(getResources().getColor(R.color.background_material_dark));
        }

        les = (ImageView)findViewById(R.id.less);
        les.setVisibility(View.GONE);
        rel = (ImageView)findViewById(R.id.real);
        rel.setVisibility(View.GONE);
        textView = (TextView)findViewById(R.id.textView);
        textView.setVisibility(View.GONE);
        final Animation fdin = AnimationUtils.loadAnimation(this, R.anim.abc_grow_fade_in_from_bottom);
        final Animation fdout = AnimationUtils.loadAnimation(this,R.anim.abc_shrink_fade_out_from_bottom);
        fdin.setDuration(1500);
        fdin.setStartOffset(500);
        fdout.setDuration(500);
        fdin.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                les.setVisibility(View.VISIBLE);
                rel.setVisibility(View.VISIBLE);
                if((ni==null)||(!ni.isConnectedOrConnecting()))
                {
                    Toast.makeText(MainActivity.this,"Please connect to the internet",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                les.startAnimation(fdout);
                rel.startAnimation(fdout);
                Log.d("Splash","Fade");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fdout.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.i("Count",Integer.toString(count));
                if(count<1)
                {
                    recreate();
                }
                else {
                      viewFlipper.showNext();
                    if(mTitle.size()!=0) {
                        getSupportActionBar().setTitle(mTitle.get(mTitle.size() - 1));
                    }
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        if(count<1)
        {
            les.startAnimation(fdin);
            rel.startAnimation(fdin);
        }
        else
        {
            viewFlipper.showNext();
            if(mTitle.size()!=0) {
                getSupportActionBar().setTitle(mTitle.get(mTitle.size() - 1));
            }
        }

  /*      if(savedInstanceState!=null)
        {
            ArrayList<Quote> qu=savedInstanceState.getParcelableArrayList("MainList");
            connectify.addToList(qu);
            Log.d("Resume", "Applied");
        }
    */
        Log.d("State", "onCreate");
        mProgress = (ProgressBar) findViewById(R.id.pBar);
        //URL constants
        mainList = (ListView)findViewById(R.id.listView1);
        //ImageView img=(ImageView)findViewById(R.id.img);
        //Log.d("bar",""+getActionBar().toString());

        //getActionBar() for future
        connectify= new Connect(mainList,this,getSupportActionBar());

        connectify.execute(url);
        Log.d("NULLS",getSupportActionBar()+"-:BAR");
        //END LOADING



        //Prep for Data Storage
        act=getApplicationContext();
        pref=getSharedPreferences("data", Context.MODE_PRIVATE);

        //Favourites
        mainList.setLongClickable(true);
        mainList.setClickable(true);
        mainList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
        public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                       int pos, long id) {
            Quote quote=connectify.getQuote(pos);
            SharedPreferences.Editor editor = pref.edit();
            int suffix=pref.getInt("Number", 0);

            editor.putString("Quote"+suffix,quote.text);
            editor.putString("Character" + suffix, quote.says);
            editor.putInt("Number", suffix + 1);
            editor.apply();
            Toast.makeText(act,"Added to favourites", Toast.LENGTH_SHORT).show();
            Log.d("long clicked", "pos: " + pos);
            return true;
        }
    });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       /* getMenuInflater().inflate(R.menu.menu_main, menu);*/
        return super.onCreateOptionsMenu(menu);
    }



    public synchronized final String computeURL()
    {
        String order="desc";
        String order_by="timestamp";
        int num=1;
        start+=1;
        return "http://www.less-real.com/api/v1/quotes?from="+start+"&num="+num+"&o="+order_by+"&o_d="+order;
    }
/*
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("Resume", "Saving");
        outState.putParcelableArrayList("MainList", connectify.getList());
        Log.d("Resume", "Saved");

    }
*/
    //MARKED future scope beyond this point
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        /*if(item.getItemId()==R.id.drawer)
        {
            if(mDrawerLayout.isDrawerOpen(Gravity.LEFT))
            {
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            }
            else {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        }*/
                // Handle your other action bar items...
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        if(mTitle.size()!=1) {
            mTitle.remove(mTitle.size()-1);
            getSupportActionBar().setTitle(mTitle.get(mTitle.size() - 1));
            super.onBackPressed();
        }
        Log.i("Back","Pressed");
    }

    /*@Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        Log.i("Key","Pressed");

        if(event.getAction()==KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_SETTINGS)
        {
            Log.i("Key","Menu");
            if(mDrawerLayout.isDrawerOpen(Gravity.LEFT))
            {
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            }
            else {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
            return true;
        }
        else {
            return super.onKeyUp(keyCode, event);
        }
    }*/

    /*

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }*/


    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
           Drit drit = (Drit)parent.getItemAtPosition(position);
           selectItem(drit.getItemstr(),position);
        }
    }

    /*Swaps fragments in the main content view */
    private void selectItem(String s, int position) {

        if(s.equals("Favorites"))
        {
            mTitle.add("Favorites");
            Stored stored = new Stored();
            getSupportFragmentManager().beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.content_frame, stored)
                    .commit();
        }
        if(s.equals("Help"))
        {
            mTitle.add("Help");
            Help help = new Help();
            getSupportFragmentManager().beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.content_frame, help)
                    .commit();
        }
        if(s.equals("Settings"))
        {
            mTitle.add("Settings");
            Settings settings = new Settings();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, settings)
                    .addToBackStack(null)
                    .commit();
        }
        if(s.equals("About"))
        {
            mTitle.add("About");
            About about = new About();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, about)
                    .addToBackStack(null)
                    .commit();
        }
        if(s.equals("Quotes"))
        {
            mTitle.removeAll(mTitle);
            mTitle.add("Quotes");
            getSupportFragmentManager().popBackStack(null, android.support.v4.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        setTitle(mTitle.get(mTitle.size()-1));

        // Highlight the selected item, update the title, and close the drawer
        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawerList);

    }

    @Override
    public void setTitle(CharSequence title) {
        if(mTitle.size()!=0) {
            getSupportActionBar().setTitle(mTitle.get(mTitle.size() - 1));
        }
    }

}
