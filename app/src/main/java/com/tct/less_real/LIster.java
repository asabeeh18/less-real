package com.tct.less_real;

import android.app.Notification;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.DragEvent;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Ahmed on 2/18/2015.
 */

public class LIster extends BaseAdapter {
    ArrayList<Quote> objList;
    Context act;
    private float x1,x2;

    public LIster() {

    }
    public LIster(ArrayList <Quote> objList,Context act)
    {
        this.act=act;
        //  this.context=context;
        //this.codeLearnLessons=codeLearnLessons;
        this.objList = objList;
        Log.d("State","Constructor:LIster");
    }
    public void setQuotes(ArrayList <Quote> quotes) {
        this.objList = quotes;
        //update the adapter to reflect the new set of movies
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return objList.size();
    }

    @Override
    public Quote getItem(int arg0) {
        // TODO Auto-generated method stub
        return objList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(int arg0, View arg1, ViewGroup arg2) {

        if(arg1==null)
        {
            LayoutInflater inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            arg1 = inflater.inflate(R.layout.lisitem2, arg2,false);
        }


        TextView animeQuote = (TextView)arg1.findViewById(R.id.quote);
        TextView animeSays = (TextView)arg1.findViewById(R.id.says);
       // TextView animeName = (TextView)arg1.findViewById(R.id.animeName);
      //  TextView animeName = (TextView)arg1.findViewById(R.id.animeName);
       // FlowTextView chapterDesc = (FlowTextView)arg1.findViewById(R.id.textView2);
        LinearLayout linearLayout = (LinearLayout)arg1.findViewById(R.id.llout);

        if(act.getSharedPreferences("Settings",Context.MODE_PRIVATE).getBoolean("Inv",false))
        {
            linearLayout.setBackgroundColor(act.getResources().getColor(R.color.primary_material_light));
            animeQuote.setTextColor(act.getResources().getColor(R.color.primary_text_default_material_light));
        }
        else
        {
            linearLayout.setBackgroundColor(act.getResources().getColor(R.color.primary_dark_material_dark));
            animeQuote.setTextColor(act.getResources().getColor(R.color.primary_text_default_material_dark));
        }

        final Quote anime = objList.get(arg0);
        //Log.d("Debug",chapter.anime);
        animeQuote.setOnTouchListener(null);


      //  chapterDesc.invalidate();
        if(anime.img!=null) {
            ImageView bM = (ImageView) arg1.findViewById(R.id.saysImage);
            bM.setImageBitmap(anime.img);
            SharedPreferences sp = act.getSharedPreferences("Settings",Context.MODE_PRIVATE);
            if(sp.getBoolean("DDLImg",false)) {
                bM.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String save = saveToInternalStorage(anime.img,anime.says);
                        Toast.makeText(act,"Image saved to "+save,Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
       else
        {
            ImageView bM = (ImageView) arg1.findViewById(R.id.saysImage);
            bM.setVisibility(View.GONE);

        }
        animeSays.setText(" ~ "+anime.says+" from "+anime.anime);
      //  animeName.setText(anime.anime);
        animeQuote.setText(anime.text);

        animeQuote.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction())
                {
                    case MotionEvent.ACTION_DOWN:
                        x1 = event.getX();
                        Log.i("getX1",Float.toString(x1));
                        break;
                    case MotionEvent.ACTION_UP:
                        x2 = event.getX();
                        Log.i("getX2",Float.toString(x2));
                        Log.i("getX2-X1",Float.toString(x2-x1));
                        if((x2 - x1)>100&&x1!=0)
                        {
                            ClipboardManager cm = (ClipboardManager)act.getSystemService(Context.CLIPBOARD_SERVICE);
                            ClipData clipData = ClipData.newPlainText("Quote",anime.text);
                            cm.setPrimaryClip(clipData);
                            Toast.makeText(act,"Quote copied to clipboard",Toast.LENGTH_SHORT).show();
                        }
                        if((x1 - x2)>100&&x1!=0)
                        {
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_SEND);
                            intent.putExtra(Intent.EXTRA_TEXT,anime.text);
                            intent.setType("text/plain");
                            act.startActivity(intent);
                        }
                        break;
                    default:
                        break;
                }

                return false;
            }
        });


      //  Log.d("State", "Got View Inflated");

        return arg1;
    }
    //SWIPE

    private String saveToInternalStorage(Bitmap bitmapImage,String name){
        ContextWrapper cw = new ContextWrapper(act.getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_WORLD_READABLE);
        // Create imageDir
        File mypath=new File(directory,name+".jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }
}
