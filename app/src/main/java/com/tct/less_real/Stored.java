package com.tct.less_real;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

public class Stored extends Fragment {
    ListView mainList;
    ArrayList<Quote> objList=new ArrayList<Quote>();

    LIster customAdapter;

    public Stored()
    {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_stored,container,false);
        //Intent intent=getIntent();
        ArrayList<Quote> quote=new ArrayList<Quote>();
        SharedPreferences pref=getActivity().getSharedPreferences("data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        SharedPreferences sp = getActivity().getSharedPreferences("Settings",Context.MODE_PRIVATE);
        RelativeLayout relativeLayout = (RelativeLayout)view.findViewById(R.id.relt);
        if(sp.getBoolean("Inv",false))
        {
            relativeLayout.setBackgroundColor(getActivity().getResources().getColor(R.color.background_material_light));
        }
        else
        {
            relativeLayout.setBackgroundColor(getActivity().getResources().getColor(R.color.background_material_dark));
        }

        int suffix=pref.getInt("Number", 0);
        for(int i=0;i<suffix;i++)
        {
            String text=pref.getString("Quote"+i,"No Data");
            String says=pref.getString("Character"+i,"No Data");

            quote.add(new Quote(text,says));
        }
        Log.d("Stored","Make it!");
        mainList = (ListView)view.findViewById(R.id.listView1);
        customAdapter = new LIster(quote,getActivity());
        mainList.setAdapter(customAdapter);

        return view;

    }
}
